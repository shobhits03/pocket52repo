package com.shobhit.pocket52assing.network

import com.shobhit.pocket52assing.models.PostModel
import com.shobhit.pocket52assing.models.UserInfo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiInterface {

    @GET
    suspend fun getUserPost(@Url url: String) : Response<List<PostModel>>

    @GET
    suspend fun getUserDetails(@Url url: String) : Response<UserInfo>
}