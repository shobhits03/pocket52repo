package com.shobhit.pocket52assing.network

class ApiConstant {

    companion object{
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
        const val POST = BASE_URL.plus("posts")
        const val USER_DETAILS  = BASE_URL.plus("users/%s")
    }
}