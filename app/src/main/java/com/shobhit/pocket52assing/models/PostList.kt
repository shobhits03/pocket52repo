package com.shobhit.pocket52assing.models

import android.os.Parcel
import android.os.Parcelable


data class PostList(
    val userId: Int,
    val postModel: List<PostModel>,
    val name: String? = null,
    val email: String? = null
) :Parcelable{
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.createTypedArrayList(PostModel)!!,
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(userId)
        parcel.writeTypedList(postModel)
        parcel.writeString(name)
        parcel.writeString(email)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PostList> {
        override fun createFromParcel(parcel: Parcel): PostList {
            return PostList(parcel)
        }

        override fun newArray(size: Int): Array<PostList?> {
            return arrayOfNulls(size)
        }
    }

}
