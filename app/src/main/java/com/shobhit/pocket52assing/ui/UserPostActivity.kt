package com.shobhit.pocket52assing.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shobhit.pocket52assing.BR
import com.shobhit.pocket52assing.R
import com.shobhit.pocket52assing.adapter.OnItemClickListner
import com.shobhit.pocket52assing.adapter.UserPostAdapter
import com.shobhit.pocket52assing.adapter.UserPostCountAdapter
import com.shobhit.pocket52assing.databinding.ActivityUserPostBinding
import com.shobhit.pocket52assing.models.DataState
import com.shobhit.pocket52assing.models.PostList
import com.shobhit.pocket52assing.models.PostModel
import com.shobhit.pocket52assing.viewmodels.MainViewModel
import com.shobhit.pocket52assing.viewmodels.UserPostViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_user_post.*


/**
 * activity to show all the post of a user
 */
@AndroidEntryPoint
class UserPostActivity : BaseActivity<UserPostViewModel, ActivityUserPostBinding>() {
    val tag = UserPostActivity::class.java.simpleName
    lateinit var post: PostList
    private val viewModel: UserPostViewModel by viewModels()
    lateinit var adapter: UserPostAdapter

    override fun getBindingVariable(): Int {
        return BR.userPost
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_user_post
    }

    override fun getViewModelBase(): UserPostViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_post)
        getIntentData()
        initRecy()
        obServeData()
    }

    private fun obServeData() {
        viewModel.userDetailsData.observe(this, Observer {
            when (it) {
                is DataState.Success -> {
                    it.data.body()?.run {
                        adapter.let {
                            it.setUserDetails(this)
                        }
                    }
                }
                is DataState.Error -> {
                    Log.d(tag, "Page encountered an error.")
                }
                DataState.Loading -> {
                    Log.d(tag, "Page is loaded")
                }
            }
        })
    }

    private fun getIntentData() {
        val intent = intent
        post = intent.getParcelableExtra("post")!!
        Log.e(tag, "post object : $post")
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUserData(post.userId)
    }

    /**
     * initialize the recyclerview and the adapter as well
     */
    private fun initRecy() {
        adapter = with(userPostList) {
            layoutManager = LinearLayoutManager(this@UserPostActivity)
            setHasFixedSize(true)
            val adapter = UserPostAdapter(post.postModel as ArrayList<PostModel>)
            setAdapter(adapter)
            adapter
        }
    }

}