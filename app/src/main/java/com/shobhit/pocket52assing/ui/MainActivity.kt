package com.shobhit.pocket52assing.ui

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shobhit.pocket52assing.BR
import com.shobhit.pocket52assing.R
import com.shobhit.pocket52assing.adapter.OnItemClickListner
import com.shobhit.pocket52assing.adapter.UserPostCountAdapter
import com.shobhit.pocket52assing.databinding.ActivityMainBinding
import com.shobhit.pocket52assing.models.DataState
import com.shobhit.pocket52assing.models.PostList
import com.shobhit.pocket52assing.models.PostModel
import com.shobhit.pocket52assing.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Response


/**
 * Activity responsible for showing the number of post per user.
 * This class uses hilt of jetpack for dependency injection
 */
@AndroidEntryPoint
class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {
    private val viewModel: MainViewModel by viewModels()
    private val tag: String = MainActivity::class.java.simpleName
    lateinit var adapter: UserPostCountAdapter

    override fun getBindingVariable(): Int {
        return BR.mainVm
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getViewModelBase(): MainViewModel {
        return viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initRecy()
        observeData()
    }

    private fun observeData() {
        viewModel.userPostData.observe(this, { it ->
            when (it) {
                is DataState.Success -> {
                    Log.d(tag, "Page is loaded")
                    groupRawData(it)
                }
                is DataState.Error -> {
                    Log.d(tag, "Page encountered an error.")
                }
                DataState.Loading -> {
                    Log.d(tag, "Page is loading")
                }
            }
        })
    }

    /**
     * function responsible for grouping raw data for each user id and displaying result
     */
    private fun groupRawData(it: DataState.Success<Response<List<PostModel>>>) {
        val userPostGroupedList = it.data.body()?.let { it ->
            it.groupBy {
                it.userId
            }
        }?.run {
            val userPostGroupedList: MutableList<PostList> = mutableListOf()
            iterator().forEach {
                Log.e(tag, "key : " + it.key + "   value  : " + it.value)
                userPostGroupedList.add(PostList(it.key, it.value))
            }
            userPostGroupedList
        }
        Log.e(tag, "grouped data : $userPostGroupedList")
        userPostGroupedList?.let {
            adapter.setData(it as ArrayList<PostList>)
        }
    }

    override fun onResume() {
        super.onResume()
        getUserPost()
    }

    /**
     * get the post of user with count
     */
    private fun getUserPost() {
        viewModel.getUserPost()
    }

    /**
     * initialize the recyclerview and the adapter as well
     */
    private fun initRecy() {
        adapter = with(userPosts) {
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
            val adapter = UserPostCountAdapter(object : OnItemClickListner {
                override fun onItemCLicked(postList: PostList) {
                    val intent = Intent(this@MainActivity,UserPostActivity::class.java)
                    intent.putExtra("post",postList)
                    startActivity(intent)
                }
            })
            setAdapter(adapter)
            adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onChanged() {
                    super.onChanged()
                    Log.e(this@MainActivity.tag, "data change called : ")
                    if (adapter.fileterdPostList.isEmpty()){
                        viewModel.isPostAvailable.set(true)
                    }else{
                        viewModel.isPostAvailable.set(false)
                    }
                }
            })
            adapter
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(
            searchManager
                .getSearchableInfo(componentName)
        )
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                adapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                adapter.filter.filter(query)
                return false
            }
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        return if (id == R.id.action_search) {
            true
        } else super.onOptionsItemSelected(item)
    }

}