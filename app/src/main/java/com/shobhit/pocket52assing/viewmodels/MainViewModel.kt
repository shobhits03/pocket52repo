package com.shobhit.pocket52assing.viewmodels

import android.util.Log
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shobhit.pocket52assing.models.DataState
import com.shobhit.pocket52assing.models.PostModel
import com.shobhit.pocket52assing.network.ApiConstant
import com.shobhit.pocket52assing.network.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import retrofit2.Response

class MainViewModel @ViewModelInject constructor(
    private val apiInterface: ApiInterface,
    @Assisted
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    val tag : String = MainViewModel::class.java.simpleName
    val userPostData: MutableLiveData<DataState<Response<List<PostModel>>>> = MutableLiveData()
    val isPostAvailable : ObservableField<Boolean> = ObservableField()


    fun getUserPost(){
        userPostData.value = DataState.Loading
        viewModelScope.async(Dispatchers.IO){
            val response = apiInterface.getUserPost(ApiConstant.POST)
            Log.d(tag, "UserPost response : $response")
            async(Dispatchers.Main){
                if (response.isSuccessful){
                    userPostData.value = DataState.Success(response)
                }else userPostData.value = DataState.Error(Throwable("Internal Server Error"))
            }
        }
    }
}