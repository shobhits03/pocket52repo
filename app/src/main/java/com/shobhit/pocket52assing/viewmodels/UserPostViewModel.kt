package com.shobhit.pocket52assing.viewmodels

import android.content.Context
import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shobhit.pocket52assing.R
import com.shobhit.pocket52assing.models.DataState
import com.shobhit.pocket52assing.models.PostModel
import com.shobhit.pocket52assing.models.UserInfo
import com.shobhit.pocket52assing.network.ApiConstant
import com.shobhit.pocket52assing.network.ApiInterface
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import retrofit2.Response

class UserPostViewModel @ViewModelInject constructor(
    private val apiInterface: ApiInterface,
    @ApplicationContext private val context: Context,
    @Assisted
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val tag: String = UserPostViewModel::class.java.simpleName
    val userDetailsData: MutableLiveData<DataState<Response<UserInfo>>> = MutableLiveData()

    fun getUserData(userId: Int) {
        userDetailsData.value = DataState.Loading
        viewModelScope.async(Dispatchers.IO) {
            val response = apiInterface.getUserDetails(ApiConstant.USER_DETAILS.format(userId))
            Log.e(tag, "getUserData : $response")
            async(Dispatchers.Main) {
                if (response.isSuccessful) {
                    userDetailsData.value = DataState.Success(response)
                } else userDetailsData.value =
                    DataState.Error(Throwable(context.getString(R.string.server_error)))
            }
        }
    }
}