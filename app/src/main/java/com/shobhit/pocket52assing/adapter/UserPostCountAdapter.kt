package com.shobhit.pocket52assing.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shobhit.pocket52assing.R
import com.shobhit.pocket52assing.databinding.UserPostCountItemBinding
import com.shobhit.pocket52assing.models.PostList
import com.shobhit.pocket52assing.models.PostModel

/**
 * adapter class used to show the user id and the number of post by databinding
 */
class UserPostCountAdapter(val onItemClickListner: OnItemClickListner) :
    RecyclerView.Adapter<UserPostCountAdapter.ViewHolder>(), Filterable {
    private var inflater: LayoutInflater? = null
    var postList: ArrayList<PostList> = ArrayList()
    var fileterdPostList: ArrayList<PostList> = ArrayList()
        get() = field


    fun setData(list: ArrayList<PostList>) {
        this.postList = list
        this.fileterdPostList = list
        notifyDataSetChanged()
    }

    class ViewHolder(userPostCountItemBinding: UserPostCountItemBinding) :
        RecyclerView.ViewHolder(userPostCountItemBinding.root) {
        var userPostCountItemBinding: UserPostCountItemBinding? = null

        init {
            this.userPostCountItemBinding = userPostCountItemBinding;
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (inflater == null) inflater = LayoutInflater.from(parent.context)
        val binding: UserPostCountItemBinding =
            DataBindingUtil.inflate(inflater!!, R.layout.user_post_count_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = holder.userPostCountItemBinding!!
        val data = fileterdPostList[position]
        binding.userId.text = data.userId.toString()
        binding.postCount.text = data.postModel.size.toString()
        binding.root.setOnClickListener {
            onItemClickListner.onItemCLicked(data)
        }
    }

    override fun getItemCount(): Int {
        return fileterdPostList.size
    }


    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    fileterdPostList = postList
                } else {
                    val resultList = ArrayList<PostList>()
                    for (row in postList) {
                        if (row.userId.toString().contains(charSearch)) {
                            resultList.add(row)
                        }
                    }
                    fileterdPostList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = fileterdPostList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                fileterdPostList = results?.values as ArrayList<PostList>
                notifyDataSetChanged()
            }
        }
    }

}

interface OnItemClickListner {
    fun onItemCLicked(postList: PostList)
}