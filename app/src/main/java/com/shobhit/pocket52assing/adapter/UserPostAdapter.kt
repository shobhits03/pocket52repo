package com.shobhit.pocket52assing.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shobhit.pocket52assing.R
import com.shobhit.pocket52assing.databinding.UserPostCountItemBinding
import com.shobhit.pocket52assing.databinding.UserPostItemBinding
import com.shobhit.pocket52assing.models.PostList
import com.shobhit.pocket52assing.models.PostModel
import com.shobhit.pocket52assing.models.UserInfo

class UserPostAdapter(private val postList: ArrayList<PostModel>) :
    RecyclerView.Adapter<UserPostAdapter.ViewHolder>() {
    private var inflater: LayoutInflater? = null
    lateinit var userInfo: UserInfo


    class ViewHolder(userPostItemBinding: UserPostItemBinding) :
        RecyclerView.ViewHolder(userPostItemBinding.root) {
        var userPostItemBinding: UserPostItemBinding? = null

        init {
            this.userPostItemBinding = userPostItemBinding;
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (inflater == null) inflater = LayoutInflater.from(parent.context)
        val binding: UserPostItemBinding =
            DataBindingUtil.inflate(inflater!!, R.layout.user_post_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = holder.userPostItemBinding!!
        val data = postList[position]
        if (::userInfo.isInitialized) {
            with(userInfo) {
                binding.name.text = name
                binding.email.text = email
            }
        }
        binding.title.text = data.title
        binding.body.text = data.body
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    fun setUserDetails(userInfo: UserInfo) {
        this.userInfo = userInfo
        notifyDataSetChanged()
    }
}